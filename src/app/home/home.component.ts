import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { TestServiceService } from '../service/test-service.service';
import { Listuser } from '../model/Listuser.model';
import { ChildrenComponent } from '../children/children.component';
import { first } from 'rxjs/operators';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  listuser: Listuser[]=[];
  bo: boolean = true;
  constructor(
    private serviceListUser : TestServiceService,

  ) { }
  userdetails: any[];
  ngOnInit(): void {
    this.getListUser();
  }

  getListUser(){
    this.serviceListUser.List_User().subscribe(
      (data)=>{
          this.listuser = data.data;
          // console.log(this.listuser);
      }
    )
  }

  deletelistuser(valuelistuser: Listuser){
    this.listuser = this.listuser.filter((item) => item.id !== valuelistuser.id);
    // console.log(this.listuser);
  }
  userDetails(valueDetails: Listuser){
    this.userdetails = this.listuser.filter((item) => item.id == valueDetails.id);
  }
  editUser(id,email,firstname,lastname){
    // console.log(id,email,firstname,lastname);
    // console.log(this.userdetails);
    this.userdetails = this.userdetails.filter((item) =>{
      item.id = id,
      item.email = email,
      item.first_name = firstname,
      item.last_name = lastname
    })
    this.bo = true;
   }
   arr;
   adduserlist(id,email,firstname,lastname,avatar){
     this.arr = 
       {
         id: id,
         email: email,
         first_name: firstname,
         last_name: lastname,
         avatar: avatar
       }
       if(this.arr.id !== ""){
        if(this.arr.email !== ""){
          if(this.arr.first_name !== ""){
            if(this.arr.avatar !== ""){
              this.listuser.push(this.arr)
            }
          }
        }
       }  
   }

   hideadduser(bool: boolean){
    // console.log(bool);
    this.bo = bool; 
   }
}
