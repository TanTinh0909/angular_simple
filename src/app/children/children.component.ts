import { Component, OnInit, Input, ViewChild, AfterViewInit, SimpleChanges, OnChanges, Output,EventEmitter } from '@angular/core';
import { HomeComponent } from '../home/home.component';
import { Listuser } from '../model/Listuser.model';
import { emit } from 'process';

@Component({
  selector: 'app-children',
  templateUrl: './children.component.html',
  styleUrls: ['./children.component.scss']
})
export class ChildrenComponent implements OnInit {

  @Input("listuser") listuser: Listuser[] = [];
  @Output("deleteusers") deleteusers = new EventEmitter<Listuser[]>();
  @Output("userdetails") userdetails = new EventEmitter<Listuser[]>();
  @Output("hideadd") hideadd = new EventEmitter<Boolean>();
  constructor() { }

  ngOnInit(): void {
  }
  // ngOnChanges(changes: SimpleChanges) {
  //   if ("listuser" in changes) {
  //     if (typeof changes["listuser"].currentValue !== "string") {
  //       // console.log(changes["listuser"].currentValue)
  //       this.listuserInput = this.listuser;
  //       // console.log(this.listuserInput);
  //     }

  //   }
  // }
  
  handDelete(deleteUser){
    // console.log(this.deleteusers.emit(deleteUser));
    this.deleteusers.emit(deleteUser)
  }
  booll: boolean = false;
  userDetails(userdetails){
    this.userdetails.emit(userdetails);;
    
  }
  kt(){
    this.hideadd.emit(this.booll);
  }
}
