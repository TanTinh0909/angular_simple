import { Injectable } from '@angular/core';

import {HttpClient} from "@angular/common/http";
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class TestServiceService {

  constructor( 
    private httpClient: HttpClient,
  ) { }

  List_User(): Observable<any>{

    return this.httpClient.get<any>('https://reqres.in/api/users')
    .pipe(tap(
        (res)=>{
        }
      )
    )
  }
}
