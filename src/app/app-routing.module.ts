import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ChildrenComponent } from './children/children.component';
import { ViewchildComponent } from './viewchild/viewchild.component';
import { NotpageComponent } from './notpage/notpage.component';


const routes: Routes = [
  {
    path:'', redirectTo:'/home',pathMatch: 'full'
  },
  {
    path:'home',component: HomeComponent
  },
  {
    path:'children',component: ChildrenComponent
  },
  {
    path:'viewchild',component: ViewchildComponent
  },
  {
    path:'**',component: NotpageComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
